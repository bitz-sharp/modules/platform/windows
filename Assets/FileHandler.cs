﻿using System;
using System.IO;
using Bitz.Modules.Core.Assets;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Debug.Logging;

namespace Bitz.Modules.Platform.Windows.Assets
{
    class FileHandler : IFileHandler
    {
        /// <inheritdoc />
        public Byte[] GetFile(String filename)
        {
            if (!File.Exists(filename))
            {
                Injector.GetSingleton<ILoggerService>().Log(LogSeverity.WARNING, $"Loading Graphics Pack from file '{filename}' failed as it was not detected on disk");
                return null;
            }
            return StreamToByteArray(GetFileStream(filename));

        }

        public Stream GetFileStream(String filename)
        {

            if (!File.Exists(filename))
            {
                Injector.GetSingleton<ILoggerService>().Log(LogSeverity.WARNING, $"Loading Graphics Pack from file '{filename}' failed as it was not detected on disk");
                return null;
            }
            if (true) return File.OpenRead(filename);

        }

        internal static Byte[] StreamToByteArray(Stream input)
        {
            Byte[] buffer = new Byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                Int32 read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        /// <inheritdoc />
        public void Initialize()
        {
        }
    }
}
