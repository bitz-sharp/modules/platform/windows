﻿using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Linq;
using Bitz.Modules.Core.Foundation.Logic;
using Bitz.Modules.Core.Graphics.Shaders;
using OpenTK;
using Boolean = System.Boolean;

namespace Bitz.Modules.Platform.Windows.Graphics.Shaders
{
    public abstract class Shader : BasicObject, IShader
    {
        private readonly List<ShaderVar> _KnownVariables = new List<ShaderVar>();

        private Boolean _IsCompiled;

        public String VertexShader { get; set; }

        public String FragementShader { get; set; }

        public Int32 ProgramID { get; private set; } = -1;

        public override void Dispose()
        {
            _KnownVariables.Clear();
            base.Dispose();
        }

        /// <summary>
        ///     Compiles this shader
        /// </summary>
        /// <returns>True is sucess else false</returns>
        internal Boolean Compile()
        {
            if (_IsCompiled || ProgramID != -1)
            {
                GL.DeleteProgram(ProgramID);
                ProgramID = 0;
            }
            ProgramID = GL.CreateProgram();

            if (!String.IsNullOrEmpty(VertexShader)) if (!CompileShader(VertexShader, ShaderType.VertexShader)) return false;
            if (!String.IsNullOrEmpty(FragementShader)) if (!CompileShader(FragementShader, ShaderType.FragmentShader)) return false;

            GL.LinkProgram(ProgramID);
            Int32 linkStatus;
            GL.GetProgram(ProgramID, GetProgramParameterName.LinkStatus, out linkStatus);
            if (linkStatus != 1)
            {
                String error = GL.GetProgramInfoLog(ProgramID);
                //todo Log link error
                _IsCompiled = false;
                return false;
            }
            _IsCompiled = true;
            return true;
        }

        /// <summary>
        ///     Compiles the given shader source using the provided type against this shaders program.
        /// </summary>
        /// <param name="source">
        ///     The source of the shader to compile respecting character restrictions as per the devices drivers
        ///     requirements
        /// </param>
        /// <param name="type">The type of shader</param>
        /// <returns>True on sucess else false</returns>
        private Boolean CompileShader(String source, ShaderType type)
        {
            Int32 shaderID = GL.CreateShader(type);
            try
            {
                GL.ShaderSource(shaderID, source);
                GL.CompileShader(shaderID);
                String logInfo = GL.GetShaderInfoLog(shaderID);
                Int32 shaderCompileStatus;
                GL.GetShader(shaderID, ShaderParameter.CompileStatus, out shaderCompileStatus);

                //todo log shader compile error

                if (shaderCompileStatus != 1) return false;

                GL.AttachShader(ProgramID, shaderID);
                GL.DeleteShader(shaderID);
                return true;
            }
            catch (Exception e)
            {
                GL.DeleteShader(shaderID);
                return false;
            }
        }

        public void SetVariable(String name, Int32 value)
        {
            InternalSetVariable(name, value, ShaderVar.VarType.INT32);
        }

        public void SetVariable(String name, Int32[] value)
        {
            InternalSetVariable(name, value, ShaderVar.VarType.INT32_ARRAY, value.Length);
        }

        public void SetVariable(String name, Single value)
        {
            InternalSetVariable(name, value, ShaderVar.VarType.SINGLE);
        }

        public void SetVariable(String name, Single[] value)
        {
            InternalSetVariable(name, value, ShaderVar.VarType.SINGLEARRAY, value.Length);
        }

        public void SetVariable(String name, Vector2 value)
        {
            InternalSetVariable(name, value, ShaderVar.VarType.VEC2);
        }

        public void SetVariable(String name, Vector2[] value)
        {
            InternalSetVariable(name, value, ShaderVar.VarType.VEC2_ARRAY, value.Length);
        }

        public void SetVariable(String name, Vector3 value)
        {
            InternalSetVariable(name, value, ShaderVar.VarType.VEC3);
        }

        public void SetVariable(String name, Vector3[] value)
        {
            InternalSetVariable(name, value, ShaderVar.VarType.VEC3_ARRAY, value.Length);
        }

        public void SetVariable(String name, Vector4 value)
        {
            InternalSetVariable(name, value, ShaderVar.VarType.VEC4);
        }

        public void SetVariable(String name, Vector4[] value)
        {
            InternalSetVariable(name, value, ShaderVar.VarType.VEC4_ARRAY, value.Length);
        }

        public void SetVariable(String name, Matrix4 value)
        {
            InternalSetVariable(name, value, ShaderVar.VarType.MAT);
        }

        private void InternalSetVariable(String name, Object value, ShaderVar.VarType type, Int32 count = 1)
        {
            ShaderVar var = _KnownVariables.FirstOrDefault(a => a.Name.Equals(name));
            if (var == null)
            {
                var = new ShaderVar
                {
                    Name = name,
                    PrevValue = null,
                    Value = value,
                    Type = type,
                    Location = -1
                };
                _KnownVariables.Add(var);
            }
            else
            {
                var.Value = value;
            }
        }

        public void Bind()
        {
            if (!_IsCompiled || ProgramID == -1) Compile();

            GL.UseProgram(ProgramID);
        }

        public abstract void PreDraw();

        public void InternalPreDraw()
        {
            if (!_IsCompiled) Compile();
            PreDraw();

            foreach (ShaderVar variable in _KnownVariables.Where(variable => variable.Location == -1 || variable.PrevValue != variable.Value))
            {
                if (variable.Location == -1) variable.Location = GL.GetUniformLocation(ProgramID, variable.Name);

                switch (variable.Type)
                {
                    case ShaderVar.VarType.INT32:
                        GL.Uniform1(variable.Location, (Int32)variable.Value);
                        break;
                    case ShaderVar.VarType.SINGLE:
                        GL.Uniform1(variable.Location, (Single)variable.Value);
                        break;
                    case ShaderVar.VarType.VEC2:
                        GL.Uniform2(variable.Location, (Vector2)variable.Value);
                        break;
                    case ShaderVar.VarType.VEC3:
                        GL.Uniform3(variable.Location, (Vector3)variable.Value);
                        break;
                    case ShaderVar.VarType.VEC4:
                        GL.Uniform4(variable.Location, (Vector4)variable.Value);
                        break;
                    case ShaderVar.VarType.MAT:
                        Matrix4 matrix = (Matrix4)variable.Value;
                        GL.UniformMatrix4(variable.Location, 1, false, ref matrix.Row0.X);
                        break;
                    case ShaderVar.VarType.INT32_ARRAY:
                        Int32[] int32Array = (Int32[])variable.Value;
                        GL.Uniform1(variable.Location, int32Array.Length, int32Array);
                        break;
                    case ShaderVar.VarType.SINGLEARRAY:
                        Single[] singleArray = (Single[])variable.Value;
                        GL.Uniform1(variable.Location, singleArray.Length, singleArray);
                        break;
                    case ShaderVar.VarType.VEC2_ARRAY:
                        Vector2[] vec2Array = (Vector2[])variable.Value;
                        Single[] vec2FlatArray = vec2Array.Select(a => new List<Single>(2) { a.X, a.Y }).SelectMany(a => a).ToArray();
                        GL.Uniform2(variable.Location, vec2Array.Length, vec2FlatArray);
                        break;
                    case ShaderVar.VarType.VEC3_ARRAY:
                        Vector3[] vec3Array = (Vector3[])variable.Value;
                        Single[] vec3FlatArray = vec3Array.Select(a => new List<Single>(2) { a.X, a.Y, a.Z }).SelectMany(a => a).ToArray();
                        GL.Uniform3(variable.Location, vec3Array.Length, vec3FlatArray);
                        break;
                    case ShaderVar.VarType.VEC4_ARRAY:
                        Vector4[] vec4Array = (Vector4[])variable.Value;
                        Single[] vec4FlatArray = vec4Array.Select(a => new List<Single>(2) { a.X, a.Y, a.Z, a.W }).SelectMany(a => a).ToArray();
                        GL.Uniform1(variable.Location, vec4Array.Length, vec4FlatArray);
                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }

                variable.PrevValue = variable.Value;
            }
        }

        internal class ShaderVar
        {
            internal Int32 Location;
            internal String Name;
            internal Object PrevValue;
            internal VarType Type;
            internal Object Value;

            internal enum VarType
            {
                INT32,
                SINGLE,
                VEC2,
                VEC3,
                VEC4,
                MAT,
                INT32_ARRAY,
                VEC2_ARRAY,
                VEC3_ARRAY,
                VEC4_ARRAY,
                SINGLEARRAY
            }
        }
    }
}