﻿using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Graphics.Shaders;
using OpenTK;

namespace Bitz.Modules.Platform.Windows.Graphics.Shaders.Stock
{
    public class Stock3D : Shader
    {
        private Vector3 _AmbientColour = Vector3.UnitX;
        private Vector3 _GlobalLightDirection = -Vector3.UnitY;

        public Stock3D()
        {
            if (ShaderService.CURRENT_SHADER_VERSION == ShaderService.ShaderVersion.DESKTOP_400)
            {
                VertexShader = @"
# version 400

" + Material.GetGLSLDefinition(10) + @"

uniform mat4 uPMatrix; 
uniform mat4 uMVMatrix;

attribute vec4 aPosition;
attribute vec4 aDiffuseColour;
attribute vec2 aTexCoordinate;
attribute vec3 aNormal;
attribute int aMaterialID;

varying vec3 vPosition;
varying vec4 vDiffuseColour;
varying vec2 vTexCoordinate;
varying vec3 vNormal;
flat out int vMaterialID;

void main()
{
    vPosition = vec3(uMVMatrix*aPosition);
    vDiffuseColour = aDiffuseColour;
    vTexCoordinate = aTexCoordinate;
    vMaterialID = aMaterialID;
    vNormal = aNormal;
    gl_Position = uPMatrix * uMVMatrix * aPosition;
}
";

                FragementShader = @"
# version 400
precision mediump float;

uniform vec3 AmbientColour;
uniform vec3 GlobalLightDirection;

" + Material.GetGLSLDefinition(10) + @"

varying vec3 vPosition;        // Interpolated position for this fragment.
varying vec4 vDiffuseColour;   // This is the diffuse color from the vertex shader interpolated across the triangle per fragment.
varying vec2 vTexCoordinate;   // Interpolated texture coordinate per fragment.
varying vec3 vNormal;
flat in int vMaterialID;

void main()
{

    vec3 ReflectingVector = reflect(-GlobalLightDirection , normalize(vNormal));
    float diffuseAOI = clamp(dot( vNormal,GlobalLightDirection ),0,1);
    float specularAOI = clamp(dot(ReflectingVector,normalize(-vPosition)),0.0,1.0);

    vec3 specular = MAT_SpecularColour[vMaterialID].xyz * pow(specularAOI,2);
    vec4 diffuse = (texture2D( MAT_DiffuseTexture[vMaterialID],vTexCoordinate)*vDiffuseColour*diffuseAOI );

    gl_FragColor = diffuse+vec4(specular,1)+vec4(AmbientColour,1);

}
";
            }


            AmbientColour = new Vector3(0.05f,0.05f,0.05f);
            GlobalLightDirection = Vector3.UnitY - Vector3.UnitX+Vector3.UnitZ;
        }

        public Vector3 AmbientColour
        {
            get => _AmbientColour;
            set
            {
                _AmbientColour = value; 
                SetVariable("AmbientColour", _AmbientColour);
            }
        }

        public Vector3 GlobalLightDirection
        {
            get => _GlobalLightDirection;
            set
            {
                _GlobalLightDirection = value;
                SetVariable("GlobalLightDirection", _GlobalLightDirection);
            }
        }

        public override void PreDraw()
        {
        }
    }
}