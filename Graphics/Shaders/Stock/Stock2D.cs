﻿using Bitz.Modules.Core.Graphics.Shaders;

namespace Bitz.Modules.Platform.Windows.Graphics.Shaders.Stock
{
    public class Stock2D : Shader
    {
        public Stock2D()
        {
            if (ShaderService.CURRENT_SHADER_VERSION == ShaderService.ShaderVersion.DESKTOP_400)
            {
                VertexShader = @"
# version 400
uniform mat4 uPMatrix; 
uniform mat4 uMVMatrix; 

attribute vec4 aPosition;
attribute vec4 aDiffuseColour;
attribute vec2 aTexCoordinate;

varying vec3 vPosition;
varying vec4 vDiffuseColour;
varying vec2 vTexCoordinate;

void main()
{
    vPosition = vec3(uMVMatrix*aPosition);
    vDiffuseColour = aDiffuseColour;    
    vTexCoordinate = aTexCoordinate;
    gl_Position = uPMatrix * uMVMatrix * aPosition;
}
";

                FragementShader = @"
# version 400
precision mediump float;

uniform sampler2D primaryTexture;

varying vec3 vPosition;        // Interpolated position for this fragment.
varying vec4 vDiffuseColour;   // This is the diffuse color from the vertex shader interpolated across the triangle per fragment.
varying vec2 vTexCoordinate;   // Interpolated texture coordinate per fragment.

void main()
{
    gl_FragColor = texture2D( primaryTexture,vTexCoordinate) * vDiffuseColour;
}
";
            }
            if (ShaderService.CURRENT_SHADER_VERSION == ShaderService.ShaderVersion.MOBILE_300)
            {
                VertexShader = @"
#version 300 es
uniform mat4 uPMatrix;
uniform mat4 uMVMatrix;

in vec4 aPosition;
in vec4 aDiffuseColour;
in vec2 aTexCoordinate;

out vec3 vPosition;
out vec4 vDiffuseColour;
out vec2 vTexCoordinate;

void main()
{
    vPosition = vec3(uMVMatrix*aPosition);
    vDiffuseColour = aDiffuseColour;
    vTexCoordinate = aTexCoordinate;
    gl_Position = uPMatrix * uMVMatrix * aPosition;
}
";

                FragementShader = @"
#version 300 es
precision mediump float;

uniform sampler2D primaryTexture;

in vec3 vPosition;        // Interpolated position for this fragment.
in vec4 vDiffuseColour;   // This is the diffuse color from the vertex shader interpolated across the triangle per fragment.
in vec2 vTexCoordinate;   // Interpolated texture coordinate per fragment.

out vec4 fragmentColor;

void main()
{
    fragmentColor = texture( primaryTexture,vTexCoordinate) * vDiffuseColour;
}
";
            }
        }

        public override void PreDraw()
        {
        }
    }
}