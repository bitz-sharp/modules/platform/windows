﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Runtime.InteropServices;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Graphics;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Platform;

namespace Bitz.Modules.Platform.Windows.Graphics
{
    public class WindowInstance : IWindowInstance
    {
        protected Vector2 _CurrentResolution;
        protected Vector2 _TargetResolution;
        protected Boolean _IsVisible;
        protected Boolean _FullScreen;
        protected Boolean _TargetFullScreen;

        private readonly NativeWindow _NativeWindow;
        private readonly GraphicsContext _Context;
        private Boolean _CloseLock = false;
        private readonly IGraphicsService _GraphicsService;

        public WindowInstance()
        {
            _GraphicsService = Injector.GetSingleton<IGraphicsService>();
            _IsVisible = false;

            _CurrentResolution = _GraphicsService.RequestedScreenLayout.Size;
            _TargetResolution = _CurrentResolution;

            if (_GraphicsService.RequestedScreenLayout.OverrideDC != IntPtr.Zero)
            {
                _NativeWindow = new NativeWindow(-40000, -40000, (Int32)_GraphicsService.RequestedScreenLayout.Size.X, (Int32)_GraphicsService.RequestedScreenLayout.Size.Y, GameInstance.Instance.GameName, GameWindowFlags.Default, GraphicsMode.Default, DisplayDevice.Default);
                SetWindowPos(_NativeWindow.WindowInfo.Handle, 1, -40000, -40000, 0, 0, 0x4 | 1 | 0x0040 | 0x0010);
                SetForegroundWindow(GetDesktopWindow());
                _NativeWindow.WindowInfo.GetType().GetField("dc", BindingFlags.GetField | BindingFlags.NonPublic | BindingFlags.Instance).SetValue(_NativeWindow.WindowInfo, _GraphicsService.RequestedScreenLayout.OverrideDC);
                _NativeWindow.WindowInfo.GetType().GetField("parent", BindingFlags.GetField | BindingFlags.NonPublic | BindingFlags.Instance).SetValue(_NativeWindow.WindowInfo, null);
                _NativeWindow.WindowInfo.GetType().GetField("handle", BindingFlags.GetField | BindingFlags.NonPublic | BindingFlags.Instance).SetValue(_NativeWindow.WindowInfo, _GraphicsService.RequestedScreenLayout.OverrideHandle);
                _NativeWindow.WindowBorder = WindowBorder.Hidden;
            }
            else
            {
                Single screenscale = 1.0f;
                _NativeWindow = new NativeWindow(10, 10, (Int32)(_CurrentResolution.X * screenscale), (Int32)(_CurrentResolution.Y * screenscale), GameInstance.Instance.GameName, GameWindowFlags.Default,GraphicsMode.Default, DisplayDevice.Default);
                _NativeWindow.Closing += NativeWindow_Closing;
                _NativeWindow.Resize += NativeWindow_Resize;
                _NativeWindow.Location = new Point(100,100);
            }

            _Context = new GraphicsContext(GraphicsMode.Default, _NativeWindow.WindowInfo, 2, 0, GraphicsContextFlags.Default);
            _Context.SwapInterval = 1;
            _Context.LoadAll();
        }

        public void SetFullScreen(Boolean active)
        {
            _TargetFullScreen = active;
        }
        public Vector2 Resolution
        {
            get => _CurrentResolution;
            set => _TargetResolution = value;
        }

        [DllImport("gdi32.dll")]
        static extern Int32 GetDeviceCaps(IntPtr hdc, Int32 nIndex);
        public enum DeviceCap
        {
            VERTRES = 10,
            DESKTOPVERTRES = 117,
        }

        [DllImport("user32.dll")]
        private static extern Boolean SetForegroundWindow(Int32 hwnd);

        [DllImport("user32.dll")]
        private static extern Int32 GetDesktopWindow();

        [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
        public static extern IntPtr SetWindowPos(IntPtr hWnd, Int32 hWndInsertAfter, Int32 x, Int32 Y, Int32 cx, Int32 cy, Int32 wFlags);


        [DllImport("user32.dll")]
        private static extern Boolean ShowWindowAsync(IntPtr hWnd, Int32 nCmdShow);

        class DesktopWIndowInfo : IWindowInfo
        {
            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public IntPtr Handle
            {
                get
                {
                    IntPtr progman = W32.FindWindow("Progman", null);

                    IntPtr result = IntPtr.Zero;

                    W32.SendMessageTimeout(progman, 0x052C, new IntPtr(0), IntPtr.Zero, W32.SendMessageTimeoutFlags.SMTO_NORMAL, 1000, out result);

                    IntPtr workerw = IntPtr.Zero;

                    // We enumerate all Windows, until we find one, that has the SHELLDLL_DefView
                    // as a child.
                    // If we found that window, we take its next sibling and assign it to workerw.
                    W32.EnumWindows(new W32.EnumWindowsProc((tophandle, topparamhandle) =>
                    {
                        IntPtr p = W32.FindWindowEx(tophandle, IntPtr.Zero, "SHELLDLL_DefView", IntPtr.Zero);

                        if (p != IntPtr.Zero) workerw = W32.FindWindowEx(IntPtr.Zero, tophandle, "WorkerW", IntPtr.Zero);

                        return true;
                    }), IntPtr.Zero);

                    // We now have the handle of the WorkerW behind the desktop icons.
                    // We can use it to create a directx device to render 3d output to it,
                    // we can use the System.Drawing classes to directly draw onto it,
                    // and of course we can set it as the parent of a windows form.
                    //
                    // There is only one restriction. The window behind the desktop icons does
                    // NOT receive any user input. So if you want to capture mouse movement,
                    // it has to be done the LowLevel way (WH_MOUSE_LL, WH_KEYBOARD_LL).


                    // Demo 1: Draw graphics between icons and wallpaper

                    // Get the Device Context of the WorkerW
                    IntPtr dc = W32.GetDCEx(workerw, IntPtr.Zero, (W32.DeviceContextValues)0x403);
                    return dc;
                }
            }
        }

        private void NativeWindow_Resize(Object sender, EventArgs e)
        {
            _TargetResolution = new Vector2(_NativeWindow.Width, _NativeWindow.Height);
            _GraphicsService.InvalidateAllCameraMatrix();
            _CurrentResolution = _TargetResolution;
            _GraphicsService.RenderSystem.UpdateViewport();
        }

        private void NativeWindow_Closing(Object sender, CancelEventArgs e)
        {
            e.Cancel = _CloseLock;
            if (!e.Cancel) GameInstance.Instance.Exit();
        }


        /// <inheritdoc />
        public Boolean Focused => _NativeWindow.Focused;

        internal INativeWindow GetNativeWindow()
        {
            return _NativeWindow;
        }

        public void Dispose()
        {
            _CloseLock = true;
            _NativeWindow.Close();
            _NativeWindow.Dispose();
            GameInstance.Instance.Exit();
            _CloseLock = false;
        }

        public void SetVisible(Boolean visible)
        {
            if (visible == _IsVisible) return;

            _IsVisible = visible;
            _NativeWindow.Visible = _IsVisible;
        }

        public IWindowInfo GetWindowInfo()
        {
            return _NativeWindow.WindowInfo;
        }

        public void ProcessWindowsEvents()
        {
            if (_TargetResolution != _CurrentResolution)
            {
                _GraphicsService.InvalidateAllCameraMatrix();
                _CurrentResolution = _TargetResolution;
                _GraphicsService.RenderSystem.UpdateViewport();
            }
            if (_TargetFullScreen && !_FullScreen)
            {
                _FullScreen = true;
                _NativeWindow.WindowState = WindowState.Fullscreen;
            }
            if (!_TargetFullScreen && _FullScreen)
            {
                _FullScreen = false;
                _NativeWindow.WindowState = WindowState.Normal;
            }
            _NativeWindow?.ProcessEvents();
        }

        public Vector2 GetWindowDimentions()
        {
            return _CurrentResolution;
        }

        public void SetSwapInterval(Int32 newInterval)
        {
            _Context.SwapInterval = newInterval;
        }

        public void SwapBuffers()
        {
            _Context.SwapBuffers();
        }

        public void MakeCurrent(IWindowInfo window)
        {
            _Context.MakeCurrent(window);
        }

        public Boolean IsWindowReady()
        {
            return true;
        }

        /// <inheritdoc />
        public void Initialize()
        {

        }
    }
}