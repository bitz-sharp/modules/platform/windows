﻿using Bitz.Modules.Core.Graphics.Cameras;
using Bitz.Modules.Core.Graphics.Canvases;
using Bitz.Modules.Platform.Windows.Graphics.Shaders.Stock;
using OpenTK.Graphics.OpenGL;
#if DESKTOP

#endif
#if ANDROID
using OpenTK.Graphics.ES30;
#endif

namespace Bitz.Modules.Platform.Windows.Graphics.Canvas
{
    /// <summary>
    /// A derivation of the Canvas class that is configured for 2D usage
    /// </summary>
    public class Canvas3D : Core.Graphics.Canvases.Canvas , ICanvas3D
    {
        public Canvas3D() : base(new Camera3D(), new Stock3D())
        {
            Visible = true;
        }

        public override void ConfigureGlState()
        {
            GL.Enable(EnableCap.Blend);
            GL.Enable(EnableCap.DepthTest);
            GL.Disable(EnableCap.StencilTest);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
        }

        /// <inheritdoc />
        public void Initialize()
        {
            
        }
    }
}
