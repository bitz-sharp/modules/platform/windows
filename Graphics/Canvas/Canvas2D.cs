﻿using System;
using Bitz.Modules.Core.Graphics.Cameras;
using Bitz.Modules.Core.Graphics.Canvases;
using Bitz.Modules.Platform.Windows.Graphics.Shaders.Stock;
using OpenTK.Graphics.OpenGL;

namespace Bitz.Modules.Platform.Windows.Graphics.Canvas
{
    /// <summary>
    /// A derivation of the Canvas class that is configured for 2D usage
    /// </summary>
    public class Canvas2D : Core.Graphics.Canvases.Canvas, ICanvas2D
    {
        public enum BlendState
        {
            ALPHA,
            ADDITIVE
        };
        public BlendState ActiveBlendState { get; set; }

        public Canvas2D() : base(new Camera2D(), new Stock2D())
        {
            Visible = true;
        }

        public Canvas2D(Camera2D cameraRef, Stock2D shaderRef) : base(cameraRef ?? new Camera2D(), shaderRef ?? new Stock2D())
        {
            Visible = true;
        }

        public override void ConfigureGlState()
        {
            GL.Enable(EnableCap.Blend);
            GL.Disable(EnableCap.DepthTest);
            GL.Disable(EnableCap.StencilTest);
            switch (ActiveBlendState)
            {
                case BlendState.ALPHA:
                    GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
                    break;
                case BlendState.ADDITIVE:
                    GL.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.One);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

        }

        /// <inheritdoc />
        public void Initialize()
        {

        }
    }
}
