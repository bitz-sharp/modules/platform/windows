﻿using System;
using Bitz.Modules.Core.Foundation;
using OpenTK;

namespace Bitz.Modules.Core.Graphics.Cameras
{
    public class Camera2D : Camera, ICamera2D
    {
        private Vector2 _Position;
        private Single _Rotation;
        private Vector2 _RotationOrigin;
        private Single _Zoom = 1;


        public Camera2D()
        { }

        public Vector2 Position
        {
            get => _Position;
            set
            {
                _Position = value;
                _ViewMatrixDirty = true;
            }
        }

        public Single PosX
        {
            get => _Position.X;
            set
            {
                _Position.X = value;
                _ViewMatrixDirty = true;
            }
        }

        public Single PosY
        {
            get => _Position.Y;
            set
            {
                _Position.Y = value;
                _ViewMatrixDirty = true;
            }
        }

        public Single Rotation
        {
            get => _Rotation;
            set
            {
                _Rotation = value;
                _ViewMatrixDirty = true;
            }
        }

        public Vector2 RotationOrigin
        {
            get => _RotationOrigin;
            set
            {
                _RotationOrigin = value;
                _ViewMatrixDirty = true;
            }
        }

        public Single Zoom
        {
            get => _Zoom;
            set
            {
                _Zoom = value;
                _ViewMatrixDirty = true;
            }
        }

        protected override void CalculateViewMatrix()
        {
            _CachedViewMatrix = Matrix4.Identity;
            _CachedViewMatrix = _CachedViewMatrix * Matrix4.LookAt(0, 0, 1, 0, 0, 0, 0, 1, 0);
            _CachedViewMatrix = _CachedViewMatrix * Matrix4.CreateTranslation(-((Int32)_ProjectionDimentions.X / 2), -((Int32)_ProjectionDimentions.Y / 2), 0);
            _CachedViewMatrix = _CachedViewMatrix * Matrix4.CreateTranslation(-_Position.X, -_Position.Y, 0);
            _CachedViewMatrix = _CachedViewMatrix * Matrix4.CreateScale(Zoom);
        }

        protected override void CalculateProjectionMatrix()
        {
            IWindowInstance window = Injector.GetSingleton<IGraphicsService>().GetWindowInstance();
            Vector2 windowDimentions = window.GetWindowDimentions();
            _CachedProjectionMatrix = Matrix4.Identity;

            switch (CurrentDimentionsMode)
            {
                case DimentionsMode.FREE:
                    _ProjectionDimentions = windowDimentions;
                    break;
                case DimentionsMode.TARGET_WIDTH:
                    {
                        Single multiplier = _TargetWidth / windowDimentions.X;
                        _ProjectionDimentions.X = _TargetWidth;
                        _ProjectionDimentions.Y = windowDimentions.Y * multiplier;
                    }
                    break;
                case DimentionsMode.TARGET_HEIGHT:
                    {
                        Single multiplier = _TargetHeight / windowDimentions.Y;
                        _ProjectionDimentions.Y = _TargetHeight;
                        _ProjectionDimentions.X = windowDimentions.X * multiplier;
                    }
                    break;
                case DimentionsMode.LOCKED:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            _CachedProjectionMatrix = _CachedProjectionMatrix * Matrix4.CreateOrthographic((Int32)_ProjectionDimentions.X, (Int32)_ProjectionDimentions.Y, 1f, 1000f);
            _CachedProjectionMatrix = _CachedProjectionMatrix * Matrix4.CreateScale(1, -1, 1);
        }
    }
}