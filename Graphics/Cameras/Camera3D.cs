﻿using System;
using Bitz.Modules.Core.Foundation;
using OpenTK;

namespace Bitz.Modules.Core.Graphics.Cameras
{
    public class Camera3D : Camera, ICamera3D
    {
        private Vector3 _Position;
        private Vector3 _Rotation;
        private Vector3 _RotationOrigin;
        private Single _Zoom = 1;

        public Vector3 Position
        {
            get => _Position;
            set
            {
                _Position = value;
                _ViewMatrixDirty = true;
            }
        }

        public Single PosX
        {
            get => _Position.X;
            set
            {
                _Position.X = value;
                _ViewMatrixDirty = true;
            }
        }

        public Single PosY
        {
            get => _Position.Y;
            set
            {
                _Position.Y = value;
                _ViewMatrixDirty = true;
            }
        }

        public Single PosZ
        {
            get => _Position.Z;
            set
            {
                _Position.Z = value;
                _ViewMatrixDirty = true;
            }
        }

        public Vector3 Rotation
        {
            get => _Rotation;
            set
            {
                _Rotation = value;
                _ViewMatrixDirty = true;
            }
        }

        public Vector3 RotationOrigin
        {
            get => _RotationOrigin;
            set
            {
                _RotationOrigin = value;
                _ViewMatrixDirty = true;
            }
        }

        public Single Zoom
        {
            get => _Zoom;
            set
            {
                _Zoom = value;
                _ViewMatrixDirty = true;
            }
        }

        protected override void CalculateViewMatrix()
        {
            _CachedViewMatrix = Matrix4.Identity;

            _CachedViewMatrix = _CachedViewMatrix * Matrix4.CreateTranslation(-_Position.X, -_Position.Y, _Position.Z);
            _CachedViewMatrix = _CachedViewMatrix * Matrix4.CreateScale(Zoom);
            _CachedViewMatrix = _CachedViewMatrix * Matrix4.CreateRotationX(Rotation.X);
            _CachedViewMatrix = _CachedViewMatrix * Matrix4.CreateRotationY(Rotation.Y);
            _CachedViewMatrix = _CachedViewMatrix * Matrix4.CreateRotationZ(Rotation.Z);
        }

        protected override void CalculateProjectionMatrix()
        {
            IWindowInstance window = Injector.GetSingleton<IGraphicsService>().GetWindowInstance();
            Vector2 windowDimentions = window.GetWindowDimentions();
            _CachedProjectionMatrix = Matrix4.Identity;

            switch (CurrentDimentionsMode)
            {
                case DimentionsMode.FREE:
                    _ProjectionDimentions = windowDimentions;
                    break;
                case DimentionsMode.TARGET_WIDTH:
                    {
                        Single multiplier = _TargetWidth / windowDimentions.X;
                        _ProjectionDimentions.X = _TargetWidth;
                        _ProjectionDimentions.Y = windowDimentions.Y * multiplier;
                    }
                    break;
                case DimentionsMode.TARGET_HEIGHT:
                    {
                        Single multiplier = _TargetHeight / windowDimentions.Y;
                        _ProjectionDimentions.Y = _TargetHeight;
                        _ProjectionDimentions.X = windowDimentions.X * multiplier;
                    }
                    break;
                case DimentionsMode.LOCKED:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            _CachedProjectionMatrix = _CachedProjectionMatrix * Matrix4.CreatePerspectiveFieldOfView((Single)(75.0/180*Math.PI), _ProjectionDimentions.X / _ProjectionDimentions.Y, 0.5f, 3000);
        }
    }
}