﻿using System;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Logic;
using OpenTK;
using Boolean = System.Boolean;

namespace Bitz.Modules.Core.Graphics.Cameras
{
    public abstract class Camera : BasicObject, ICamera
    {
        public enum DimentionsMode
        {
            FREE,
            TARGET_WIDTH,
            TARGET_HEIGHT,
            LOCKED
        }

        protected Matrix4 _CachedProjectionMatrix;
        protected Matrix4 _CachedViewMatrix;


        protected Boolean _ProjectionMatrixDirty = true;
        protected Boolean _ViewMatrixDirty = true;
        protected Vector2 _ProjectionDimentions;
        protected DimentionsMode _CurrentDimentionsMode;
        protected Single _TargetWidth;
        protected Single _TargetHeight;

        public Single Width
        {
            get
            {
                if (_ProjectionMatrixDirty)
                {
                    CalculateProjectionMatrix();
                    _ProjectionMatrixDirty = false;
                }
                return _ProjectionDimentions.X;
            }
            set => _ProjectionDimentions.X = value;
        }

        public Single Height
        {
            get
            {
                if (_ProjectionMatrixDirty)
                {
                    CalculateProjectionMatrix();
                    _ProjectionMatrixDirty = false;
                }
                return _ProjectionDimentions.Y;
            }
            set => _ProjectionDimentions.Y = value;
        }

        public Vector2 Size
        {
            get
            {
                if (_ProjectionMatrixDirty)
                {
                    CalculateProjectionMatrix();
                    _ProjectionMatrixDirty = false;
                }
                return _ProjectionDimentions;
            }
            set => _ProjectionDimentions = value;
        }

        protected Camera()
        {
            Injector.GetSingleton<IGraphicsService>().RegisterCamera(this);
        }

        public Matrix4 ViewMatrix
        {
            get
            {
                if (_ViewMatrixDirty)
                {
                    CalculateViewMatrix();
                    _ViewMatrixDirty = false;
                }
                Matrix4 returnValue = _CachedViewMatrix;
                return returnValue;
            }
        }

        public Matrix4 ProjectionMatrix
        {
            get
            {
                if (_ProjectionMatrixDirty)
                {
                    CalculateProjectionMatrix();
                    _ProjectionMatrixDirty = false;
                }
                Matrix4 returnValue = _CachedProjectionMatrix;
                return returnValue;
            }
        }

        protected DimentionsMode CurrentDimentionsMode
        {
            get => _CurrentDimentionsMode;
            set => _CurrentDimentionsMode = value;
        }

        protected abstract void CalculateViewMatrix();

        protected abstract void CalculateProjectionMatrix();

        public void InvalidateMatrix()
        {
            _ProjectionMatrixDirty = true;
            _ViewMatrixDirty = true;
        }

        public override void Dispose()
        {
            Injector.GetSingleton<IGraphicsService>().UnRegisterCamera(this);
            base.Dispose();
        }

        public void SetModeFree()
        {
            _CurrentDimentionsMode = DimentionsMode.FREE;
            _ProjectionMatrixDirty = true;
        }

        public void SetModeTargetWidth(Single targetWidth)
        {
            _TargetWidth = targetWidth;
            _CurrentDimentionsMode = DimentionsMode.TARGET_WIDTH;
            _ProjectionMatrixDirty = true;
        }

        public void SetModeTargetHeight(Single targetHeight)
        {
            _TargetHeight = targetHeight;
            _CurrentDimentionsMode = DimentionsMode.TARGET_HEIGHT;
            _ProjectionMatrixDirty = true;
        }

        public void SetModeLocked()
        {
            _CurrentDimentionsMode = DimentionsMode.LOCKED;
            _ProjectionMatrixDirty = true;
        }

        public Vector4 UnProject(Vector2 position)
        {
            Vector4 vec;

            Vector2 windowSize = Injector.GetSingleton<IGraphicsService>().GetWindowInstance().GetWindowDimentions();

            vec.X = 2.0f * position.X / (Single)windowSize.X - 1;
            vec.Y = -(2.0f * position.Y / (Single)windowSize.Y - 1);
            vec.Z = 0;
            vec.W = 1.0f;

            Matrix4 viewInv = Matrix4.Invert(ViewMatrix);
            Matrix4 projInv = Matrix4.Invert(ProjectionMatrix);

            Vector4.Transform(ref vec, ref projInv, out vec);
            Vector4.Transform(ref vec, ref viewInv, out vec);

            if (vec.W > Single.Epsilon || vec.W < Single.Epsilon)
            {
                vec.X /= vec.W;
                vec.Y /= vec.W;
                vec.Z /= vec.W;
            }

            return vec;
        }

        /// <inheritdoc />
        public void Initialize()
        {
        }
    }
}