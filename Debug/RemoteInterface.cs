﻿using System;
using Bitz.Modules.Core.Debug.Remoting;
using Sbatman.Networking.Client;
using Sbatman.Serialize;

namespace Bitz.Modules.Platform.Windows.Debug
{
    class RemoteInterface : IRemoteInterface
    {
        private BaseClient _NetClient;

        public void SendMessage(RemoteMessage message)
        {
            if (!_NetClient.Connected) return;

            Packet outgoingPacket = new Packet((UInt16)4568);
            outgoingPacket.Add((Int16)message.ID);
            outgoingPacket.Add(message.Message ?? " ");

            _NetClient.SendPacket(outgoingPacket);
        }

        public void Connect(String host, Int32 port)
        {
            _NetClient = new BaseClient();
            _NetClient.Connect(host, port);
        }

        public void Disconnect()
        {
            _NetClient?.Disconnect();
            _NetClient = null;
        }

        /// <inheritdoc />
        public void Initialize()
        {
        }

        /// <inheritdoc />
        public Boolean TryUpdate(TimeSpan currentInstanceTime)
        {
            return false;
        }

        /// <inheritdoc />
        public void Dispose()
        {
            Disconnect();
        }
    }
}
