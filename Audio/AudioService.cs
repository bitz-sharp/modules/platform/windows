﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bitz.Modules.Core.Audio;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Debug.Logging;
using Bitz.Modules.Core.Foundation.Services;
using OpenTK.Audio;
using OpenTK.Audio.OpenAL;

namespace Bitz.Modules.Platform.Windows.Audio
{
    public class AudioService : Service, IAudioService
    {
        private AudioContext _AlContext;

        private readonly Object _LockingObject = new Object();
        /// <summary> Maximum amount of Channels </summary>
        private const Int32 MAXCHANNELS = 16;
        /// <summary> Array of all the source IDS </summary>
        private Int32[] _SourceIds;
        /// <summary> List of all reserved channels </summary>
        private readonly List<Channels> _ReservedChannels = new List<Channels>();

        private readonly Dictionary<Channels, AudioPlayer> _Channels = new Dictionary<Channels, AudioPlayer>();

        private readonly List<AudioData> _AudioData = new List<AudioData>();

        public override void Initialize()
        {
            _AlContext = new AudioContext();
            _SourceIds = AL.GenSources(MAXCHANNELS);

            if (DebugMode) Injector.GetSingleton<ILoggerService>().Log(LogSeverity.VERBOSE, $"Audio Service Init with {MAXCHANNELS} channels");

            for (Int32 i = 0; i < MAXCHANNELS; i++)
            {
                _Channels.Add((Channels)i, null);
            }
            RegisterWithUpdateService();
        }

        protected override void Update(TimeSpan timeSinceLastUpdate)
        {
            foreach (AudioPlayer audioPlayer in _Channels.Values.ToList())
            {
                audioPlayer?.Update();
            }
        }

        protected override void Shutdown()
        {
            StopAllSounds();
            UnRegisterWithUpdateService();
            AL.DeleteSources(_SourceIds);
            _AlContext.Dispose();
        }

        public void StopAllSounds()
        {
            foreach (AudioPlayer audioPlayer in _Channels.Values)
            {
                if (DebugMode) Injector.GetSingleton<ILoggerService>().Log(LogSeverity.VERBOSE, $"Stopping Audio Player channel:{audioPlayer.Channel}, sourceID:{audioPlayer.SourceId}");
                audioPlayer?.Stop();
            }
        }

        public void LoadSound(String path)
        {
            if (_AudioData.FirstOrDefault(s => s.FilePath == path) != null)
            {
                if (DebugMode) Injector.GetSingleton<ILoggerService>().Log(LogSeverity.VERBOSE, $"Attempted to Load Loaded Sound:\n{path}");
                return;
            }

            _AudioData.Add(new AudioData(path));
        }

        internal void RegisterAudioPlayer(AudioPlayer player)
        {
            if (player.Channel != Channels.CHANNEL_ANY)
            {
                if (DebugMode) Injector.GetSingleton<ILoggerService>().Log(LogSeverity.VERBOSE, $"Registering audio player with specific channel, channel:{player.Channel}, sourceID:{player.SourceId}");
                _Channels[player.Channel]?.Stop();

                _Channels[player.Channel] = player;
                player.SourceId = _SourceIds[(Int32)player.Channel];
                player.AssignedChannel = player.Channel;
            }
            else
            {
                for (Int32 i = 0; i < MAXCHANNELS; i++)
                {
                    if (_ReservedChannels.Contains((Channels)i) || _Channels[(Channels)i] != null) continue;
                    
                    player.AssignedChannel = (Channels)i;
                    _Channels[player.AssignedChannel] = player;
                    RecycleSource(player.AssignedChannel);
                    player.SourceId = _SourceIds[i];

                    if (DebugMode) Injector.GetSingleton<ILoggerService>().Log(LogSeverity.VERBOSE, $"Registering audio player with any channel, channel:{player.AssignedChannel}, sourceID:{player.SourceId}");
                    return;
                }

                Injector.GetSingleton<ILoggerService>().Log(LogSeverity.WARNING, "No free Channels for Audioplayer");
            }

        }

        internal void UnregisterAudioPlayer(AudioPlayer player)
        {
            if (player == null) return;
            if (DebugMode) Injector.GetSingleton<ILoggerService>().Log(LogSeverity.VERBOSE, $"Unregistering audio player, channel:{player.AssignedChannel}, sourceID:{player.SourceId}");
            _Channels[player.AssignedChannel]?.Stop();
            _Channels[player.AssignedChannel] = null;
        }


        public override String GetServiceDebugStatus()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append($"C:{_Channels.Where(a => a.Value != null)}");
            return stringBuilder.ToString();
        }

        public void ReserveChannel(Channels channel)
        {
            if (DebugMode) Injector.GetSingleton<ILoggerService>().Log(LogSeverity.VERBOSE, $"Reserving channel: {(Int32)channel}");
            if (!_ReservedChannels.Contains(channel)) _ReservedChannels.Add(channel);
        }

        public void UnreserveChannel(Channels channel)
        {
            if (DebugMode) Injector.GetSingleton<ILoggerService>().Log(LogSeverity.VERBOSE, $"Unreserving channel: {(Int32)channel}");
            if (_ReservedChannels.Contains(channel)) _ReservedChannels.Remove(channel);
        }

        /// <summary> Returns data for a loaded sound file </summary>
        /// <param name="path">Path of the sound you want</param>
        /// <returns></returns>
        public IAudioData GetData(String path)
        {
            AudioData data = _AudioData.FirstOrDefault(s => s.FilePath == path);
            if (data == null) { throw new Exception($"No Loaded Sound with Path: {path}"); }

            return data;
        }

        internal void RecycleSource(Channels assignedChannel)
        {
            AL.DeleteSource(_SourceIds[(Int32)assignedChannel]);
            _SourceIds[(Int32)assignedChannel] = AL.GenSource();
        }

        public IAudioPlayer CreateAudioPlayer(String path, Boolean playOnStart = false, Channels channel = Channels.CHANNEL_ANY, Boolean looping = false, Boolean disposeOnFinish = true)
        {
            return new AudioPlayer(path, playOnStart, channel, looping, disposeOnFinish);
        }
    }
}