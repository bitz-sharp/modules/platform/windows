﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Debug.Logging;
using Bitz.Modules.Core.Graphics;
using Bitz.Modules.Core.Input;
using OpenTK;
using OpenTK.Input;

namespace Bitz.Modules.Platform.Windows.Input.InputSources
{
    internal class KeyboardInput : IInputSource
    {
        private KeyboardState _PrevKeyboardState;
        private KeyboardState _CurrentKeyboardState;
        public void ProcessInputEvents(List<InputEvent> activeEvents)
        {
            _CurrentKeyboardState = Keyboard.GetState();

            Boolean currentlyFocuesd = Injector.GetSingleton<IGraphicsService>().GetWindowInstance().Focused;

            for (Int32 i = 0; i < 128; i++)TestKey(activeEvents, (Key) i, currentlyFocuesd && _CurrentKeyboardState[(Key)i], _PrevKeyboardState[(Key)i]);

            foreach (InputEvent inputEvent in activeEvents.ToArray().Where(inputEvent => inputEvent.TypeOfInput == InputEvent.InputType.KEY).Where(inputEvent => !_CurrentKeyboardState[(Key) inputEvent.Data]))
            {
                inputEvent.Complete();
                activeEvents.Remove(inputEvent);
            }

            _PrevKeyboardState = _CurrentKeyboardState;
        }

        private static void TestKey(ICollection<InputEvent> activeEvents, Key key, Boolean state, Boolean prevState)
        {
            if (state && !prevState)
            {
                InputEvent keyEvent = new InputEvent
                {
                    Position = new Vector2(Mouse.GetCursorState().X, Mouse.GetCursorState().Y),
                    Active = true,
                    StartPosition = new Vector2(Mouse.GetCursorState().X, Mouse.GetCursorState().Y),
                    TypeOfInput = InputEvent.InputType.KEY,
                    Data = key
                };
                activeEvents.Add(keyEvent);

                Injector.GetSingleton<ILoggerService>().Log(LogSeverity.VERBOSE, $"Keyboard {key} Pressed ");
            }
        }

  

        /// <inheritdoc />
        public void Dispose()
        {
        }

        /// <inheritdoc />
        public void Initialize()
        {
        }
    }
}
