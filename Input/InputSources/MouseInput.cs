﻿using System;
using System.Collections.Generic;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Debug.Logging;
using Bitz.Modules.Core.Graphics;
using Bitz.Modules.Core.Input;
using Bitz.Modules.Platform.Windows.Graphics;
using OpenTK;
using OpenTK.Input;

namespace Bitz.Modules.Platform.Windows.Input.InputSources
{
    class MouseInput : IInputSource
    {
        private InputEvent _MouseInputEvent;
        private MouseState _PreviousMouseState;
        private ILoggerService _LoggerService;
        private Vector2 _MousePosition;
        private MouseState _MouseState;
        private Boolean _WindowHooked;

        public MouseInput()
        {
            _LoggerService = Injector.GetSingleton<ILoggerService>();
        }

        public void ProcessInputEvents(List<InputEvent> activeEvents)
        {
            if (!_WindowHooked)
            {
                IWindowInstance window = Injector.GetSingleton<IGraphicsService>().RenderSystem?.GetWindowInstance();
                INativeWindow nativeWindow = (window as WindowInstance)?.GetNativeWindow();
                if (nativeWindow == null) return;
                nativeWindow.MouseMove += NativeWindow_MouseMove;
                nativeWindow.MouseDown += NativeWindow_MouseDown;
                nativeWindow.MouseUp += NativeWindow_MouseUp;
                _WindowHooked = true;
            }

            MouseState mouseState = _MouseState;

            Boolean currentlyFocused = Injector.GetSingleton<IGraphicsService>().GetWindowInstance().Focused;
            TestButton(activeEvents, "Left", currentlyFocused ? mouseState.LeftButton : ButtonState.Released, _PreviousMouseState.LeftButton, InputEvent.InputType.LEFT_CLICK);
            TestButton(activeEvents, "Right", currentlyFocused ? mouseState.RightButton : ButtonState.Released, _PreviousMouseState.RightButton, InputEvent.InputType.RIGHT_CLICK);
            TestButton(activeEvents, "Middle", currentlyFocused ? mouseState.MiddleButton : ButtonState.Released, _PreviousMouseState.MiddleButton, InputEvent.InputType.MIDDLE_CLICK);

            _PreviousMouseState = mouseState;
        }

        private void NativeWindow_MouseUp(Object sender, MouseButtonEventArgs e)
        {
            _MousePosition.X = e.X;
            _MousePosition.Y = e.Y;
            _MouseState = e.Mouse;
        }

        private void NativeWindow_MouseDown(Object sender, MouseButtonEventArgs e)
        {
            _MousePosition.X = e.X;
            _MousePosition.Y = e.Y;
            _MouseState = e.Mouse;
        }

        private void NativeWindow_MouseMove(Object sender, MouseMoveEventArgs e)
        {
            _MousePosition.X = e.X;
            _MousePosition.Y = e.Y;
            _MouseState = e.Mouse;
        }

        private void TestButton(ICollection<InputEvent> activeEvents, String buttonName, ButtonState state, ButtonState prevState, InputEvent.InputType actionType)
        {
            INativeWindow window = (Injector.GetSingleton<IGraphicsService>().RenderSystem?.GetWindowInstance() as WindowInstance)?.GetNativeWindow();
            Single borderY = window.ClientSize.Height - (Single)window.Size.Height;
            Single borderX = window.ClientSize.Width - (Single)window.Size.Width;
           
            Vector2 mousePos = new Vector2(Mouse.GetCursorState().X, Mouse.GetCursorState().Y);
            mousePos -= new Vector2(window.X, window.Y);
            mousePos.X += borderX;
            mousePos.Y += borderY;

            mousePos.X = window.InputDriver.Mouse[0].X;
            mousePos.Y = window.InputDriver.Mouse[0].Y;

            switch (state)
            {
                case ButtonState.Pressed when prevState == ButtonState.Released:
                    _MouseInputEvent = new InputEvent
                    {
                        Position = mousePos,
                        Active = true,
                        StartPosition = mousePos,
                        TypeOfInput = actionType
                    };
                    activeEvents.Add(_MouseInputEvent);
                    _LoggerService.Log(LogSeverity.VERBOSE, $"Mouse {buttonName} Input Press @ X:{_MouseInputEvent.Position.X} Y:{_MouseInputEvent.Position.Y}");
                    break;

                case ButtonState.Pressed when _MouseInputEvent != null:
                    _MouseInputEvent.Position = mousePos;
                    break;

                case ButtonState.Released when prevState == ButtonState.Pressed && _MouseInputEvent != null:
                    _MouseInputEvent.Complete();
                    activeEvents.Remove(_MouseInputEvent);
                    _LoggerService.Log(LogSeverity.VERBOSE, $"Mouse {buttonName} Input Release @ X:{_MouseInputEvent.Position.X} Y:{_MouseInputEvent.Position.Y}");
                    break;
            }
        }

        /// <inheritdoc />
        public void Initialize()
        {
        }

        /// <inheritdoc />
        public void Dispose()
        {
            _LoggerService = null;
        }
    }
}